/** @jsx React.DOM */
var React = require('react/addons');
var $ = require('jquery');

require('./style.css')

var Sandwich = React.createClass({

	componentDidMount: function() {
		require('../../_libs/jquery.knob.js')
		var self = this;
		var $knob = $(this.refs.knob.getDOMNode());
		this.$knob = $knob;
		$knob.knob({
			angleOffset: 0,
			lineCap: 'round',
			fgColor: '#CC2E30',
			font: 'Roboto',
			fontWeight: 'normal',
			fontSize: '4rem',
			width: 150,
			height: 150,
			bgColor: '#FFF',
			fillBg: true,
			fillPadding: 10,
			thickness: .1,
			change: function(value) {
				var val = Math.round(value);
				if (self.props.value !== value) {
					self.props.onChange(value);
				}
			}
		});
	},

	componentWillReceiveProps: function(nextProps) {
		if (this.$knob) {
			this.$knob.val(nextProps.value).trigger('change');
		}
	},

	render: function() {
		return (
			<div className="gcSandwich">
				<input type="text" value={this.props.value} ref="knob" />
				<div className="gcSandwichCaption">SANDWICHES</div>
			</div>
		);
	}

});

module.exports = Sandwich;