/** @jsx React.DOM */
var React = require('react/addons');

require('./style.css');
var Header = React.createClass({

	render: function() {
		return (
			<div className="dcHeader">
				<div className="dcHeaderMeat">MEAT</div>
				<div className="dcHeaderAmp">&amp;</div>
				<div className="dcHeaderCheese">CHEESE</div>
			</div>
		);
	}

});

module.exports = Header;