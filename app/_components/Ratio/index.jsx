/** @jsx React.DOM */
var React = require('react/addons');
var $ = require('jquery');
var SliderInput = require('../SliderInput')


// require("imports?$=jquery!../../_libs/jquery.nouislider.js")
require('./style.css');
var Ratio = React.createClass({

	render: function() {
		return (
			<div className="dcRatio">
				<SliderInput 
					className="dcRatioMeat"
					onChange={this.props.onMeatRatioChange} 
					value={this.props.meat}
				/>
				<div className="dcRatioCaption">RATIO</div>
				<SliderInput 
					className="dcRatioCheese"
					onChange={this.props.onCheeseRatioChange}
					value={this.props.cheese}
				/>
				{this.props.children}
			</div>
		);
	}

});

module.exports = Ratio;