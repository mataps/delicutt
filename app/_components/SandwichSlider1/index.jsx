/** @jsx React.DOM */
var React = require('react/addons');
var Slider = require('react-slider');

require('./style.css');

React.initializeTouchEvents(true);
var ReactSlider = React.createFactory(Slider);

var SandwichSlider1 = React.createClass({

	render: function() {
		var slider = ReactSlider({
      className: 'dcSandwichSlider1',
      orientation: 'horizontal',
      withBars: true,
      value: this.props.value,
      onChange: this.props.onChange,
      minDistance: 0
    });

		return (
			<div className="dcSandwichSlider1Container">
				{slider}
				<div className="dcSandwichSlider1Value">{this.props.value}</div>
				<div className="dcSandwichSlider1Dash"></div>
			</div>
		);
	}

});

module.exports = SandwichSlider1;