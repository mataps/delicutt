/** @jsx React.DOM */
var React = require('react/addons');

require('./style.css');
var SliderInput = React.createClass({

	render: function() {
		return (
			<div className={"dcSliderInput " + this.props.className}>
				<a href="#" className="dcSliderInputInc" onClick={this.handleInc}>
					<i className="ion-android-arrow-dropup"></i>
				</a>
				<input type="text" className="dcSliderInputEl" value={this.props.value} />
				<a href="#" className="dcSliderInputDec" onClick={this.handleDec}>
					<i className="ion-android-arrow-dropdown"></i>
				</a>
			</div>
		);
	},

	handleInc: function (e) {
		e.preventDefault();
		var value = Number(Math.round10(this.props.value, -2) + 1).toFixed(2);
		if (value >= 0) {
			this.props.onChange(Number(value));
		}
	},

	handleDec: function (e) {
		e.preventDefault();
		var value = Number(Math.round10(this.props.value, -2) - 1).toFixed(2);
		if (value >= 0) {
			this.props.onChange(Number(value));
		}
	},

	handleChange: function(e) {
		e.preventDefault();
		if(isNaN(e.target.value) || e.target.value < 0) {
			return;
		}
		var value = Math.round10(e.target.value, -2);
		this.props.onChange(value);
	}

});

module.exports = SliderInput;