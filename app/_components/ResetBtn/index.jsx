/** @jsx React.DOM */
var React = require('react/addons');

require('./style.css');
var ResetBtn = React.createClass({

	render: function() {
		return (
			<a href="#" className="dcResetBtn" onClick={this.props.onClick}>RESET</a>
		);
	}

});

module.exports = ResetBtn;