/** @jsx React.DOM */
var React = require('react/addons');
var Link = require('react-router').Link;

require('./style.css');

var Nav = React.createClass({

	getInitialState: function() {
		return {
			themes: [
				{
					name: 'Theme 1',
					route: 'theme1'
				},
				{
					name: 'Theme 2',
					route: 'theme2'
				}
			]
		};
	},

	render: function() {
		var themes = this.state.themes.map(function(theme, i) {
			return (
				<li key={i}>
					<Link to={theme.route} >{theme.name}</Link>
				</li>
			);
		}.bind(this));

		return (
			<nav className="dcMenu">
				<h2><i className="ion-social-html5"></i> Pages</h2>
				<ul className="dcPagesList">
					<li><a href="/pages/1">Page 1</a></li>
					<li><a href="/pages/2">Page 2</a></li>
					<li><a href="/pages/3">Page 3</a></li>
					<li><a href="/pages/4">Page 4</a></li>
					<li><a href="/pages/5">Page 5</a></li>
				</ul>
				<div className="dcThemeSelector">
					<h2><i className="ion-android-color-palette"></i> Themes</h2>
					<ul>
						{themes}
					</ul>
				</div>
			</nav>
		);
	}

});

module.exports = Nav;