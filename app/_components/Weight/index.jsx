/** @jsx React.DOM */
var React = require('react/addons');
var SliderInput = require('../SliderInput');

require('./style.css')
var Weight = React.createClass({

	render: function() {
		return (
			<div className="dcWeight">
				<SliderInput 
					className="dcWeightMeat"
					onChange={this.props.onMeatWeightChange}
					value={this.props.meat}
				/>
				<div className="dcWeightCaption">LBS.</div>
				<SliderInput 
					className="dcWeightCheese"
					onChange={this.props.onCheeseWeightChange}
					value={this.props.cheese}
				/>
			</div>
		);
	}

});

module.exports = Weight;