/** @jsx React.DOM */
var React = require('react/addons');

require('./style.css');
var Footer = React.createClass({

	render: function() {
		return (
			<div className="dcFooter">
				<div className="dcFooterLeft">
					<a href="#" className="dcMenuToggle"><i className="ion-drag"></i></a>
				</div>
				<div className="dcFooterCaption">
					<a href="/">DELICALCULATOR.COM</a>
				</div>
				<div className="dcFooterRight">
					<a href="#"><i className="ion-social-twitter"></i></a>
				</div>
			</div>
		);
	}
});

module.exports = Footer;