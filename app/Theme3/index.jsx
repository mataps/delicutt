/** @jsx React.DOM */
var React = require('react/addons');
var Nav = require('../_components/Nav');
var Header = require('../_components/Header');
var Ratio = require('../_components/Ratio');
var Weight = require('../_components/Weight');
var Footer = require('../_components/Footer');
var Sandwich = require('../_components/Sandwich');
var $ = require('jquery');

require('normalize.css/normalize.css');
require('./style.css');

var Homepage = React.createClass({

	componentDidMount: function() {
  	$toggle = $('.dcMenuToggle');
  	$overlay = $('.dcPusherOverlay');
  	$app = $('.dcApp');
  	$toggle.on('click', function(e) {
  		$app.addClass('dcMenuOpen');
  	});
  	$overlay.on('click', function(e) {
  		$app.removeClass('dcMenuOpen');
  	});
  },

	getInitialState: function() {
		return {
			meat: {
				ratio: 3,
				weight: 1.25
			},
			cheese: {
				ratio: 1,
				weight: 1.05
			},
			sandwich: 24
		};
	},

	render: function() {
		return (
			<div className="dcContentWrapper dcHomepage">
				<Nav />
				<div className="dcPusher">
					<Header />
					<Ratio 
						meat={this.state.meat.ratio}
						cheese={this.state.cheese.ratio}
						onChangeMeatRatio={this.onChangeMeatRatio}
						onChangeCheeseRatio={this.onChangeCheeseRatio}
					/>
					<Sandwich value={this.state.sandwich}/>
					<Weight
						meat={this.state.meat.weight} 
						cheese={this.state.cheese.weight}
						onChangeMeatWeight={this.handleChangeMeatWeight}
						onChangeCheeseWeight={this.handleChangeCheeseWeight}
					/>
					<Footer />
					<div className="dcPusherOverlay"></div>
				</div>
			</div>
		);
	},

	onChangeMeatRatio: function(value) {
		var state = this.state;
		state.meat.ratio = value;
		state.meat.weight = this.calculateMeatWeight(value);
		this.setState(state);
	},

	onChangeCheeseRatio: function(value) {
		var state = this.state;
		state.cheese.ratio = value;
		state.cheese.weight = this.calculateCheeseWeight(value);
		this.setState(state);
	},

	handleChangeMeatWeight: function(value) {
		var state = this.state;
		state.meat.weight = value;
		state.meat.ratio = this.calculateMeatRatio(value);
		this.setState(state);
	},

	handleChangeCheeseWeight: function(value) {
		var state = this.state;
		state.cheese.weight = value;
		state.cheese.ratio = this.calculateCheeseRatio(value);
		this.setState(state);
	},

	calculateMeatWeight: function(ratio) {
		var value = ratio * 0.4166666666666667;
		return Math.round10(value, -2);
	},

	calculateCheeseWeight: function(ratio) {
		var value = ratio * 1.05;
		return Math.round10(value, -2);
	},

	calculateMeatRatio: function(weight) {
		var value = weight / 0.4166666666666667;
		return Math.round10(value, -2);
	},

	calculateCheeseRatio: function(weight) {
		var value = weight / 1.05;
		return Math.round10(value, -2);
	}

});

module.exports = Homepage;