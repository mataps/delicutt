require('../../config/_libs/math-round');

module.exports = {

	getWeight: function(ratio, sandwich, perSlice) {
		var value = ratio * (sandwich || 1) * perSlice;
		return Math.round10(Math.round10(value, -2) / 10, -2);
	},

	getRatio: function(weight, sandwich, perSlice) {
		var value = weight / (sandwich || 1) / perSlice;
		return Math.round10(Math.round10(value, -2) * 10, -2);
	}

};