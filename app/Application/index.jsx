var React = require("react/addons");
var StateFromStoreMixin = require("items-store/StateFromStoresMixin");
var RouteHandler = require("react-router").RouteHandler;
var cx = React.addons.classSet;
var $ = require('jquery');
var calc = require('./calculations');

require("./style.css");

var Application = React.createClass({
	mixins: [StateFromStoreMixin],
	statics: {
		getState: function(stores, params) {
			var transition = stores.Router.getItem("transition");
			return {
				loading: !!transition
			};
		},
	},

	getInitialState: function() {
		var state = {
			meat: {
				ratio: 2.5,
				perSlice: 0.75
			},
			cheese: {
				ratio: 1,
				perSlice: 0.70
			},
			sandwich: 10
		};
		state.meat.weight = calc.getWeight(state.meat.ratio, state.sandwich, state.meat.perSlice);
		state.cheese.weight = calc.getWeight(state.cheese.ratio, state.sandwich, state.cheese.perSlice);
		return state;
	},

	render: function() {
		var classes = cx({
			'loading': this.state.loading
		});
		return (
			<div className={'dcApp' + classes}>
				{this.state.loading ? <div style={{float: "right"}}>loading...</div> : null}
				<RouteHandler 
					meatRatio={this.state.meat.ratio}
					meatWeight={this.state.meat.weight}
					cheeseRatio={this.state.cheese.ratio}
					cheeseWeight={this.state.cheese.weight}
					sandwich={this.state.sandwich}
					onSandwichChange={this.handleSandwichChange}
					onMeatRatioChange={this.handleMeatRatioChange}
					onMeatWeightChange={this.handleMeatWeightChange}
					onCheeseRatioChange={this.handleCheeseRatioChange}
					onCheeseWeightChange={this.handleCheeseWeightChange}
					onReset={this.handleReset}
				/>
			</div>
		);
	},

	handleMeatRatioChange: function(value) {
		var state = this.state;
		var ratio = state.meat.ratio = value;
		var sandwich = state.sandwich;
		var perSlice = state.meat.perSlice;
		state.meat.weight = calc.getWeight(ratio, sandwich, perSlice);
		this.setState(state);
	},

	handleCheeseRatioChange: function(value) {
		var state = this.state;
		var ratio = state.cheese.ratio = value;
		var sandwich = state.sandwich;
		var perSlice = state.cheese.perSlice;
		state.cheese.weight = calc.getWeight(ratio, sandwich, perSlice);
		this.setState(state);
	},

	handleMeatWeightChange: function(value) {
		var state = this.state;
		var weight = state.meat.weight = value;
		var sandwich = state.sandwich;
		var perSlice = state.meat.perSlice;
		state.meat.ratio = calc.getRatio(weight, sandwich, perSlice);
		this.setState(state);
	},

	handleCheeseWeightChange: function(value) {
		var state = this.state;
		var weight = state.cheese.weight = value;
		var sandwich = state.sandwich;
		var perSlice = state.cheese.perSlice;
		state.cheese.ratio = calc.getRatio(weight, sandwich, perSlice);
		this.setState(state);
	},

	handleSandwichChange: function(value) {
		var state = this.state;
		state.sandwich = value;
		this.setState(state);
		this.handleMeatRatioChange(state.meat.ratio);
		this.handleCheeseRatioChange(state.cheese.ratio);
	},

	handleReset: function(e) {
		e.preventDefault();
		this.setState(this.getInitialState());
	}

});
module.exports = Application;