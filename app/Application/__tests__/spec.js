jest.dontMock('../calculations.js');

var calc = require('../calculations.js');
var meatSlice = .75;
var cheeseSlice = .70;

describe('Application calculations', function() {
  
  it('should calculate correct weight', function() {
    var ratio, sandwich, result, weightResult, RatioResult;

    //initial was 1.87 RESULT is 1.88
    ratio = 2.5;
    sandwich = 10;
    weightResult = calc.getWeight(ratio, sandwich, meatSlice);
    ratioResult = calc.getRatio(weightResult, sandwich, meatSlice);
    expect(weightResult).toBe(1.88);
    expect(ratioResult).toBe(ratio);

    //initial was 1.75 RESULT is 1.4
    ratio = 2;
    sandwich = 10;
    weightResult = calc.getWeight(ratio, sandwich, cheeseSlice);
    ratioResult = calc.getRatio(weightResult, sandwich, cheeseSlice);
    expect(weightResult).toBe(1.4);
    expect(ratioResult).toBe(ratio);

    ratio = 2;
    sandwich = 27;
    weightResult = calc.getWeight(ratio, sandwich, meatSlice);
    ratioResult = calc.getRatio(weightResult, sandwich, meatSlice);
    expect(weightResult).toBe(4.05);
    expect(ratioResult).toBe(ratio);

    ratio = 1;
    sandwich = 27;
    weightResult = calc.getWeight(ratio, sandwich, cheeseSlice);
    ratioResult = calc.getRatio(weightResult, sandwich, cheeseSlice);
    expect(weightResult).toBe(1.89);
    expect(ratioResult).toBe(ratio);

    //initial was 1.12 RESULT is 1.13
    ratio = 3;
    sandwich = 5;
    weightResult = calc.getWeight(ratio, sandwich, meatSlice);
    ratioResult = calc.getRatio(weightResult, sandwich, meatSlice);
    expect(weightResult).toBe(1.13);
    expect(ratioResult).toBe(ratio);
  })

})