/** @jsx React.DOM */
var React = require('react/addons');
var Nav = require('../_components/Nav');
var Header = require('../_components/Header');
var Ratio = require('../_components/Ratio');
var Weight = require('../_components/Weight');
var Footer = require('../_components/Footer');
var Sandwich = require('../_components/Sandwich');
var ResetBtn = require('../_components/ResetBtn');
var $ = require('jquery');

require('normalize.css/normalize.css');
require('./style.less');

var Homepage = React.createClass({

	componentDidMount: function() {
  	$toggle = $('.dcMenuToggle');
  	$overlay = $('.dcPusherOverlay');
  	$app = $('.dcApp');
  	$toggle.on('click', function(e) {
  		$app.addClass('dcMenuOpen');
  	});
  	$overlay.on('click', function(e) {
  		$app.removeClass('dcMenuOpen');
  	});
  },

	render: function() {
		return (
			<div className="theme1">
				<Nav />
				<div className="dcPusher">
					<div className="dcContentWrapper">
							<Header />
							<Weight
								meat={this.props.meatWeight}
								cheese={this.props.cheeseWeight}
								onMeatWeightChange={this.props.onMeatWeightChange}
								onCheeseWeightChange={this.props.onCheeseWeightChange}
							/>
							<Sandwich value={this.props.sandwich} onChange={this.props.onSandwichChange} />
							<Ratio 
								meat={this.props.meatRatio}
								cheese={this.props.cheeseRatio}
								onMeatRatioChange={this.props.onMeatRatioChange}
								onCheeseRatioChange={this.props.onCheeseRatioChange}
							>
								<ResetBtn onClick={this.props.onReset}/>
							</Ratio>
							<Footer />
					</div>
					<div className="dcPusherOverlay"></div>
				</div>
			</div>
		);
	}

});

module.exports = Homepage;