var React = require("react");
var Router = require("react-router");
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;

// export routes
module.exports = (
	<Route name="app" path="/" handler={require("./Application")}>
		<Route name="theme3" handler={require("./Theme3")} />
		<Route name="theme2" handler={require("./Theme2")} />
		<DefaultRoute name="theme1" handler={require("./Theme1")} />
	</Route>
);
